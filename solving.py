
"""
Combinatorial optimization tools to find and count
solutions satisfying some tests
"""

#############################################################################
# Imports ###################################################################

import os
import sys
import subprocess
import tempfile
import itertools
import numpy as np
import pulp
import fairdiv

from networkx import max_weight_matching
from networkx.algorithms import bipartite
from networkx.algorithms.bipartite.matching import maximum_matching

from scipy import sparse


#############################################################################
# Constants #################################################################

# Do not forget to define these constants in config.py
try:
    from config import CPLEX_PATH
    from config import SAT4J_COMMAND
except ModuleNotFoundError:
    sys.stderr.write("""
CPLEX_PATH and SAT4J_COMMAND are not configured. You should set these
constants in config.py (you can use config_template.py as a template for
config.py).
""")
    sys.exit(1)


#############################################################################
# Backtracking ##############################################################

def find_solution(instance, is_solution, cut=lambda allocation: False):
    """
    Runs a backtracking algorithm to find an allocation that
    satisfies is_solution. The function cut is used to prune
    branches of the tree (by default, does not prune anything).

    The function returns the allocation found, if any, or None
    if no solution exists.
    """
    return _find_solution(fairdiv.Allocation(instance), 0, is_solution, cut)


def _find_solution(allocation, k, is_solution, cut):
    if k == allocation.nb_objects:  # leaf
        if is_solution(allocation):
            return allocation
        return None
    else:  # internal node
        if cut(allocation):
            return None
        for i in range(0, allocation.nb_agents):
            allocation.give(i, k)
            solution = _find_solution(allocation, k + 1, is_solution, cut)
            if solution is not None:  # solution found!
                return solution
            else:  # backtrack
                allocation.take_back(i, k)
    return None


def count_solutions(instance, tests, scale=True, debug=False,
                    cut=lambda allocation: False):
    """
    Runs a backtracking algorithm to count the number of allocations
    that satisfy the list of tests passed in parameters.

    Returns:
    A list of integers. The first integer is the total number of instances tested
    (number of leaves of the search tree). The ith number (for i > 0) is the number
    of allocations passing test i-1.

    Arguments:
    -- the instance
    -- the list of tests (boolean functions)

    Keyword arguments:
    scale -- if this argument is True, then it means that the tests are
             of increasing strength (hence at a leaf, if a test fails, we
             do not need to do the subsequent tests) -- default True.
    cut   -- a Boolean function indicating whether we can prune a branch
             -- does not prune anything by default.
    """
    count = _count_solutions(fairdiv.Allocation(instance), 0, tests,
                             [0] * (len(tests) + 1), scale, debug, cut)
    if debug:
        sys.stderr.write('\n')
    return count


def _count_solutions(allocation, k, tests, counts, scale, debug, cut):
    if k == allocation.nb_objects:  # leaf
        counts[0] += 1
        if debug:
            _show_progress(counts[0],
                           allocation.nb_agents ** allocation.nb_objects)
        for i, test in enumerate(tests):
            current_test = test(allocation)
            counts[i + 1] += int(current_test)
            if scale and not current_test:
                break
        return counts
    if cut(allocation):  # cut
        return 0
    for i in range(allocation.nb_agents):
        allocation.give(i, k)
        _count_solutions(allocation, k + 1, tests, counts, scale, debug, cut)
        allocation.take_back(i, k)
    return counts


def _show_progress(current, total):
    screen_total = 80
    screen_current = (current * screen_total) // total
    sys.stderr.write('\r|' + ('=' * (screen_current)))
    sys.stderr.write((' ' * (screen_total - screen_current)))
    sys.stderr.write('| {} / {}'.format(current, total))


#############################################################################
# Max-min share #############################################################

def max_min_share(instance, agent, nb_shares=0):
    """Returns the utility of the max-min-share for a given agent."""
    nb_agents = nb_shares if nb_shares else instance.nb_agents
    nb_objects = instance.nb_objects

    # 1. We define the variables
    x = []
    for ag in range(nb_agents):
        x.append(pulp.LpVariable.dicts("x_{}".format(ag), range(nb_objects),
                                       0, 1, pulp.LpInteger))
    u_min = pulp.LpVariable("u_min", 0, None, pulp.LpContinuous)

    # 2. We define the problem with the objective
    prob = pulp.LpProblem("Max-min share computation", pulp.LpMaximize)
    prob += u_min

    # 3. We define the constraints
    # 3.1 The utility min is lower than the utility of all the shares
    for ag in range(nb_agents):
        prob += (pulp.lpSum([x[ag][obj] * instance.w[agent][obj]
                             for obj in range(nb_objects)]) >= u_min)
    # 3.2 Each object should be allocated only once
    for obj in range(nb_objects):
        prob += (pulp.lpSum([x[ag][obj] for ag in range(nb_agents)]) == 1)

    prob.solve(pulp.CPLEX(path=CPLEX_PATH))

    assert prob.status == pulp.LpStatusOptimal,\
        "The solver was not able to solve the max-min problem optimally..."
    return u_min.varValue


def min_max_share(instance, agent, nb_shares=0):
    """Returns the utility of the min-max-share for a given agent."""
    nb_agents = nb_shares if nb_shares else instance.nb_agents
    nb_objects = instance.nb_objects

    # 1. We define the variables
    x = []
    for ag in range(nb_agents):
        x.append(pulp.LpVariable.dicts("x_{}".format(ag), range(nb_objects),
                                       0, 1, pulp.LpInteger))
    u_max = pulp.LpVariable("u_max", 0, None, pulp.LpContinuous)

    # 2. We define the problem with the objective
    prob = pulp.LpProblem("Min-max share computation", pulp.LpMinimize)
    prob += u_max

    # 3. We define the constraints
    # 3.1 The utility max is greater than the utility of all the shares
    for ag in range(nb_agents):
        prob += (pulp.lpSum([x[ag][obj] * instance.w[agent][obj]
                             for obj in range(nb_objects)]) <= u_max)
    # 3.2 Each object should be allocated only once
    for obj in range(nb_objects):
        prob += (pulp.lpSum([x[ag][obj] for ag in range(nb_agents)]) == 1)

    prob.solve(pulp.CPLEX(path=CPLEX_PATH))

    assert prob.status == pulp.LpStatusOptimal,\
        "The solver was not able to solve the min-max problem optimally..."
    return u_max.varValue


#############################################################################
# CEEI ######################################################################

def better_than(instance, agent, threshold, strict=True):
    """Generator of all the shares for agent a that have
    a better utility than the threshold."""
    if strict:
        return (share
                for share in itertools.product([True, False],
                                               repeat=instance.nb_objects)
                if instance.utility(agent, share) > threshold)
    return (share
            for share in itertools.product([True, False],
                                           repeat=instance.nb_objects)
            if instance.utility(agent, share) >= threshold)


def compute_ceei_prices(allocation):
    """Computes the set of prices in a Competitive Equilibrium from Equal Incomes,
    and returns None if no such equilibrium exists."""
    nb_agents = allocation.instance.nb_agents
    nb_objects = allocation.instance.nb_objects

    # 1. We define the variables
    prices = pulp.LpVariable.dicts("p", range(nb_objects),
                                   0, None, pulp.LpContinuous)
    d = pulp.LpVariable("d", 0, None, pulp.LpInteger)

    # 2. We define the problem with the objective
    prob = pulp.LpProblem("CEEI Price Computation", pulp.LpMinimize)
    prob += d

    # 3. We define the constraints
    # 3.1 The prices of the current allocation should not exceed d
    for agent in range(nb_agents):
        prob += (pulp.lpSum([allocation.pi[agent][obj] * prices[obj]
                             for obj in range(nb_objects)]) <= d)
    # 3.2 The prices of all the better shares for a given agent exceed d
    current_utilities = allocation.utility_vect()
    for agent in range(nb_agents):
        for share in better_than(allocation.instance,
                                 agent, current_utilities[agent]):
            prob += (pulp.lpSum(
                [share[obj] * prices[obj]
                 for obj in range(nb_objects)]) >= (d + 1))

    prob.solve(pulp.CPLEX(path=CPLEX_PATH))

    if prob.status == pulp.LpStatusOptimal:
        return [prices[obj].varValue / d.varValue for obj in range(nb_objects)]
    return None


#############################################################################
# Pareto ####################################################################

def is_pareto_optimal(allocation):
    """Returns the utility of the min-max-share for a given agent."""
    nb_agents = allocation.instance.nb_agents
    nb_objects = allocation.instance.nb_objects
    old_u = allocation.utility_vect()
    old_uc = sum(old_u)

    # 1. We define the variables
    x = []
    for ag in range(nb_agents):
        x.append(pulp.LpVariable.dicts("x_{}".format(ag), range(nb_objects),
                                       0, 1, pulp.LpInteger))
    u_c = pulp.LpVariable("uc", 0, None, pulp.LpContinuous)
    # 2. We define the problem with the objective
    prob = pulp.LpProblem("Pareto-optimal test", pulp.LpMaximize)
    prob += u_c

    # 3. We define the constraints
    # 3.1 Each object should be allocated only once
    for obj in range(nb_objects):
        prob += (pulp.lpSum([x[ag][obj] for ag in range(nb_agents)]) == 1)
    # 3.2 The new utility should be greater than the old one for each agent 
    for ag in range(nb_agents):
        prob += (pulp.lpSum([x[ag][obj] * allocation.instance.w[ag][obj]
                             for obj in range(nb_objects)]) >= old_u[ag])
    # 3.3 Computation of the collective utility
    prob += (pulp.lpSum([x[ag][obj] * allocation.instance.w[ag][obj]
                         for obj in range(nb_objects)
                         for ag in range(nb_agents)]) >= u_c)
    # 3.4 The new global utility should be strictly greater
    prob += (u_c >= old_uc + 1)

    prob.solve(pulp.CPLEX(path=CPLEX_PATH))

    return prob.status != pulp.LpStatusOptimal


#############################################################################
# K-app envy ################################################################

def is_k_app_envy(instance, threshold):
    """
Returns False if and only if the instance is not threshold-app EF.
Otherwise, returns the allocation that is threshold-app EF.
Solves this problem by resorting to an external Pseudo-Boolean
solver (Sat4J)."""
    nb_agents = instance.nb_agents
    nb_objects = instance.nb_objects

    # Variables
    offset = 1
    x = [[offset + nb_objects * ag + ob
         for ob in range(nb_objects)]
         for ag in range(nb_agents)]
    offset += nb_objects * nb_agents
    e = [[[offset + nb_agents * (nb_agents * ag1 + ag2) + ag3
           for ag3 in range(nb_agents)]
          for ag2 in range(nb_agents)]
         for ag1 in range(nb_agents)]
    offset += nb_agents * nb_agents * nb_agents

    # Constraints
    constraints = []
    for ob in range(nb_objects):
        constraints.append(
            " ".join("+1 x{}".format(
                x[ag][ob])
                for ag in range(nb_agents))
            + " = 1")
    for k in range(nb_agents):
        for i in range(nb_agents):
            for h in range(nb_agents):
                constraints.append(
                    " ".join("+{} x{}".format(
                        int(instance.w[k][j]),
                        x[h][j])
                        for j in range(nb_objects))
                    + " " +
                    " ".join("-{} x{}".format(
                        int(instance.w[k][j]),
                        x[i][j])
                        for j in range(nb_objects))
                    + " " +
                    " ".join("+{} x{} x{}".format(
                        int(instance.w[k][j]),
                        x[i][j],
                        e[k][i][h])
                        for j in range(nb_objects))
                    + " " +
                    " ".join("-{} x{} x{}".format(
                        int(instance.w[k][j]),
                        x[h][j],
                        e[k][i][h])
                        for j in range(nb_objects))
                    + " <= 0"
                    )
    for i in range(nb_agents):
        for h in range(nb_agents):
            constraints.append(
                " ".join("+1 x{} x{}".format(
                    e[i][i][h],
                    e[k][i][h])
                    for k in range(nb_agents))
                + " <= " + str(threshold)
            )

    # Objective (dummy)
    objective = "min: +1 x1"

    tmp = tempfile.NamedTemporaryFile(delete=False)
    try:
        tmp.write(bytes(
            "* #variable= {} #constraint= {}\n*\n".format(
                offset,
                len(constraints)),
            "utf-8")
        )
        tmp.write(bytes(objective + ";\n", "utf-8"))
        for cons in constraints:
            tmp.write(bytes(cons + ";\n", "utf-8"))
    finally:
        tmp.close()

    process = subprocess.run(SAT4J_COMMAND.split(' ') + [tmp.name],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for row in process.stdout.decode("utf-8").split("\n"):
        if row and row[0] == 's':
            if row.find('SATISFIABLE') == -1:
                return False
        if row and row[0] == 'v':
            values = [0 if lit and lit[0] == '-' else 1
                      for lit in (row[2:].strip()).split(" ")]
            return fairdiv.Allocation(
                instance,
                [[values[x[ag][ob] - 1] for ob in range(nb_objects)]
                 for ag in range(nb_agents)])
    os.unlink(tmp.name)


def compute_best_k_app_envy(instance):
    """
Computes the lowest k for which instance is k-app EF.
Returns the allocation guaranteeing this k-app EF, and
the value of k. Solves this problem by a sequence of calls
to is_k_app_envy."""
    for k in range(0, instance.nb_agents + 1):
        current_sol = is_k_app_envy(instance, k)
        if current_sol:
            return current_sol, k
    # We are not supposed to be here (because the last call
    # to is_k_app_envy should return a solution)
    assert False, "Something is wrong with the computation of k-app EF"


def compute_best_k_app_envy_2(instance):
    """
Computes the lowest k for which instance is k-app EF.
Returns the allocation guaranteeing this k-app EF, and
the value of k. Solves this problem as an optimization
problem by resorting to an external Pseudo-Boolean solver
(Sat4J)."""
    nb_agents = instance.nb_agents
    nb_objects = instance.nb_objects

    # Variables
    offset = 1
    x = [[offset + nb_objects * ag + ob
         for ob in range(nb_objects)]
         for ag in range(nb_agents)]
    offset += nb_objects * nb_agents
    e = [[[offset + nb_agents * (nb_agents * ag1 + ag2) + ag3
           for ag3 in range(nb_agents)]
          for ag2 in range(nb_agents)]
         for ag1 in range(nb_agents)]
    offset += nb_agents * nb_agents * nb_agents
    dummies = [offset + ag for ag in range(nb_agents)]
    offset += nb_agents

    # Constraints
    constraints = []
    for ob in range(nb_objects):
        constraints.append(
            " ".join("+1 x{}".format(
                x[ag][ob])
                for ag in range(nb_agents))
            + " = 1")
    for k in range(nb_agents):
        for i in range(nb_agents):
            for h in range(nb_agents):
                constraints.append(
                    " ".join("+{} x{}".format(
                        instance.w[k][j],
                        x[h][j])
                        for j in range(nb_objects))
                    + " " +
                    " ".join("-{} x{}".format(
                        instance.w[k][j],
                        x[i][j])
                        for j in range(nb_objects))
                    + " " +
                    " ".join("+{} x{} x{}".format(
                        instance.w[k][j],
                        x[i][j],
                        e[k][i][h])
                        for j in range(nb_objects))
                    + " " +
                    " ".join("-{} x{} x{}".format(
                        instance.w[k][j],
                        x[h][j],
                        e[k][i][h])
                        for j in range(nb_objects))
                    + " <= 0"
                    )
    for i in range(nb_agents):
        for h in range(nb_agents):
            constraints.append(
                " ".join("+1 x{} x{}".format(
                    e[i][i][h],
                    e[k][i][h])
                    for k in range(nb_agents))
                + " " + " ".join("-1 x{}".format(dummies[ag])
                                 for ag in range(nb_agents))
                + " <= 0"
            )

    # Objective (dummy)
    objective = "min: " + " ".join("+1 x{}".format(dummies[ag])
                                   for ag in range(nb_agents))

    tmp = tempfile.NamedTemporaryFile(delete=False)
    try:
        tmp.write(bytes(
            "* #variable= {} #constraint= {}\n*\n*\n".format(
                offset,
                len(constraints)),
            "utf-8")
        )
        tmp.write(bytes(objective + ";\n", "utf-8"))
        for cons in constraints:
            tmp.write(bytes(cons + ";\n", "utf-8"))
    finally:
        tmp.close()

    process = subprocess.run(SAT4J_COMMAND.split(' ') + [tmp.name],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for row in process.stdout.decode("utf-8").split("\n"):
        if row and row[0] == 's':
            if row.find('OPTIMUM FOUND') == -1:
                return None, instance.nb_agents + 1
        if row and row[0] == 'v':
            values = [0 if lit and lit[0] == '-' else 1
                      for lit in (row[2:].strip()).split(" ")]
            best_k = sum(values[dummies[ag] - 1] for ag in range(nb_agents))
            return (fairdiv.Allocation(
                instance,
                [[values[x[ag][ob] - 1] for ob in range(nb_objects)]
                 for ag in range(nb_agents)]),
                    best_k)
    os.unlink(tmp.name)


#############################################################################
# K-app prop ################################################################

def is_k_app_prop(instance, threshold):
    """
Returns False if and only if the instance is not threshold-app prop.
Otherwise, returns the allocation that is threshold-app prop.
Solves this problem by resorting to an external Pseudo-Boolean
solver (Sat4J)."""
    nb_agents = instance.nb_agents
    nb_objects = instance.nb_objects

    # Variables
    offset = 1
    x = [[offset + nb_objects * ag + ob
         for ob in range(nb_objects)]
         for ag in range(nb_agents)]
    offset += nb_objects * nb_agents
    p = [[offset + nb_agents * ag + ob
          for ob in range(nb_agents)]
         for ag in range(nb_agents)]
    offset += nb_agents * nb_agents

    # Constraints
    constraints = []
    for ob in range(nb_objects):
        constraints.append(
            " ".join("+1 x{}".format(
                x[ag][ob])
                for ag in range(nb_agents))
            + " = 1")
    for k in range(nb_agents):
        prop_k = sum(instance.w[k][j] for j in range(nb_objects))
        for i in range(nb_agents):
            constraints.append(
                " ".join("+{} x{}".format(
                    int(nb_agents * instance.w[k][j]),
                    x[i][j])
                    for j in range(nb_objects))
                + " " +
                " ".join("-{} x{} x{}".format(
                    int(nb_agents * instance.w[k][j]),
                    x[i][j],
                    p[k][i])
                    for j in range(nb_objects))
                + " " +
                "+{} x{}".format(
                    int(prop_k),
                    p[k][i])
                + " >= {}".format(int(prop_k))
            )
    for i in range(nb_agents):
        constraints.append(
            " ".join("+1 x{} x{}".format(
                p[i][i],
                p[k][i])
                     for k in range(nb_agents))
            + " <= " + str(threshold)
        )

    # Objective (dummy)
    objective = "min: +1 x1"

    tmp = tempfile.NamedTemporaryFile(delete=False)
    try:
        tmp.write(bytes(
            "* #variable= {} #constraint= {}\n*\n".format(
                offset,
                len(constraints)),
            "utf-8")
        )
        tmp.write(bytes(objective + ";\n", "utf-8"))
        for cons in constraints:
            tmp.write(bytes(cons + ";\n", "utf-8"))
    finally:
        tmp.close()

    process = subprocess.run(SAT4J_COMMAND.split(' ') + [tmp.name],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for row in process.stdout.decode("utf-8").split("\n"):
        if row and row[0] == 's':
            if row.find('SATISFIABLE') == -1:
                return False
        if row and row[0] == 'v':
            values = [0 if lit and lit[0] == '-' else 1
                      for lit in (row[2:].strip()).split(" ")]
            return fairdiv.Allocation(
                instance,
                [[values[x[ag][ob] - 1] for ob in range(nb_objects)]
                 for ag in range(nb_agents)])
    os.unlink(tmp.name)


def compute_best_k_app_prop(instance):
    """
Computes the lowest k for which instance is k-app prop.
Returns the allocation guaranteeing this k-app prop, and
the value of k. Solves this problem by a sequence of calls
to is_k_app_prop."""
    for k in range(0, instance.nb_agents + 1):
        current_sol = is_k_app_prop(instance, k)
        if current_sol:
            return current_sol, k
    # We are not supposed to be here (because the last call
    # to is_k_app_envy should return a solution)
    assert False, "Something is wrong with the computation of k-app EF"


#############################################################################
# K-app envy (alternative - useless - notion) ###############################

def compute_best_alternative_k_app_envy(instance):
    """
    Runs a backtracking algorithm to find an allocation that
    minimizes the k-app envy for the alternative notion of envy.

    More precisely:
    - for each allocation
    - for each envious agent
    - count the number of agents that agree with this envy
    - try to minimize this envy
    """

    def compute_kappef(allocation, debug=False):
        envious_agents = (i for i in range(allocation.nb_agents)
                          if allocation.envy_vector()[i] > 0)
        kappef = 1  # by default, noone agrees with the envy
        utility_matrix = allocation.utility_matrix()
        top_utilities = np.amax(utility_matrix, 1)
        for envious_agent in envious_agents:
            if debug:
                print("Envious agent: {}".format(envious_agent))
                print("Utility: {}".format(allocation.utility_vect()[envious_agent]))
            agree_with_envy = sum(
                (1
                 for other_agent in range(allocation.nb_agents)
                 if utility_matrix[other_agent, envious_agent] < top_utilities[other_agent]
                 )
            )
            if debug:
                for other_agent in range(allocation.nb_agents):
                    if utility_matrix[other_agent, envious_agent] < top_utilities[other_agent]:
                        print("Agent {} also thinks {} is envious".format(other_agent,
                                                                          envious_agent))
                        print("Because {} < {}".format(
                            utility_matrix[other_agent, envious_agent],
                            top_utilities[other_agent]
                        ))
            kappef = max(kappef, agree_with_envy)
        return kappef

    def compute_aux(allocation, k, lb, ub, current_solution):
        if k == allocation.nb_objects:  # leaf
            lb = compute_kappef(allocation)
            if lb < ub:
                current_solution.pi = np.copy(allocation.pi)
                return current_solution, lb
            return None
        else:  # internal node
            for i in range(0, allocation.nb_agents):
                allocation.give(i, k)
                rec_call = compute_aux(allocation, k + 1, lb, ub, current_solution)
                if rec_call is not None:
                    current_solution, ub = rec_call
                allocation.take_back(i, k)
        return current_solution, ub

    return compute_aux(fairdiv.Allocation(instance), 0, 0, instance.nb_agents + 1,
                       fairdiv.Allocation(instance))


#############################################################################
# K-app envy for house allocation problems ##################################


def compute_best_k_app_envy_HAP(instance):
    """
Computes the lowest k for which this HAP instance is k-app EF.
Returns the allocation guaranteeing this k-app EF, and
the value of k. The instance should be HAP (i.e nb_agents == nb_objects)."""
    nb_agents = instance.nb_agents
    nb_objects = instance.nb_objects

    assert nb_agents == nb_objects, "This instance is not HAP!"

    nb_env = np.zeros((nb_objects, nb_objects))

    # nb_env[obj1, obj2] denotes the number of agents that prefer
    # obj2 to obj1
    for obj1 in range(nb_objects):
        for obj2 in range(obj1 + 1, nb_objects):
            for ag in range(nb_agents):
                if instance.w[ag][obj2] > instance.w[ag][obj1]:
                    nb_env[obj1, obj2] += 1
                if instance.w[ag][obj2] < instance.w[ag][obj1]:
                    nb_env[obj2, obj1] += 1

    max_envy = np.zeros((nb_agents, nb_objects))

    # max_envy[ag, obj] denotes the maximal value of nb_env[obj, obj2] among
    # the objects obj2 that are strictly preferred to obj by ag
    for ag in range(nb_agents):
        for obj in range(nb_objects):
            max_envy[ag, obj] = (
                0
                if instance.w[ag][obj] == max(instance.w[ag][obj2] for obj2 in range(nb_objects))
                else max(nb_env[obj, obj2]
                         for obj2 in range(nb_objects)
                         if instance.w[ag][obj2] > instance.w[ag][obj]))

    allocation = None
    lower, upper = 0, nb_agents

    # First check whether there is an EF allocation
    kappef = 0
    nbEnvOnes = (max_envy <= kappef).astype(int)
    graph = bipartite.from_biadjacency_matrix(sparse.csr_matrix(nbEnvOnes))
    matching = max_weight_matching(graph)
    if len(matching) == nb_agents:
        return allocation, 1

    # If there is no, proceed with a dichotomous search
    while lower <= upper:
        middle = (lower + upper) // 2
        nbEnvOnes = (max_envy <= middle).astype(int)
        graph = bipartite.from_biadjacency_matrix(sparse.csr_matrix(nbEnvOnes))
        matching = max_weight_matching(graph)
        if len(matching) == nb_agents:
            allocation = matching
            kappef = middle
            upper = middle - 1
        else:
            lower = middle + 1
    return allocation, kappef


def compute_best_k_app_prop_HAP(instance):
    """
Computes the lowest k for which this HAP instance is k-app nonpropF.
Returns the allocation guaranteeing this k-app nonpropF, and
the value of k. The instance should be HAP (i.e nb_agents == nb_objects)."""
    nb_agents = instance.nb_agents
    nb_objects = instance.nb_objects

    assert nb_agents == nb_objects, "This instance is not HAP!"

    # prop[ag] denotes agent ag's proportional share
    prop = np.sum(instance.w, 1) / nb_agents

    nonprop = np.zeros((nb_agents, nb_objects))
    # nonprop[obj] denotes the number of agents that think
    # object j is lower than their proportional share
    for ag in range(nb_agents):
        for obj in range(nb_objects):
            nonprop[ag, obj] = (
                0
                if instance.w[ag][obj] >= prop[ag]
                else sum(1 for i in range(nb_agents) if instance.w[i, obj] < prop[i]))
    allocation = None
    lower, upper = 0, nb_agents

    # First check whether there is a prop allocation
    kappNP = 0
    nbNPOnes = (nonprop <= kappNP).astype(int)
    graph = bipartite.from_biadjacency_matrix(sparse.csr_matrix(nbNPOnes))
    matching = max_weight_matching(graph)
    if len(matching) == nb_agents:
        return allocation, 1

    # If there is no, proceed with a dichotomous search
    while lower <= upper:
        middle = (lower + upper) // 2
        nbNPOnes = (nonprop <= middle).astype(int)
        graph = bipartite.from_biadjacency_matrix(sparse.csr_matrix(nbNPOnes))
        matching = max_weight_matching(graph)
        if len(matching) == nb_agents:
            allocation = matching
            kappNP = middle
            upper = middle - 1
        else:
            lower = middle + 1
    return allocation, kappNP
