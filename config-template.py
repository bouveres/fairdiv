
"""Configuration constants for the fairdiv package."""

CPLEX_PATH = ""  # example: /path/to/cplex/bin/x86-64_sles10_4.1/cplex"
SAT4J_COMMAND = ""  # example: "java -jar /path/to/sat4j/sat4j-pb.jar"
